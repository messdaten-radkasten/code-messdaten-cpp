# Code Messdatenerfassung C++

## Dependencies

```bash
sudo apt-get install clang-17 clang++-17 clang-tools-17 libclang-cpp17-dev cmake git build-essential pipx mold gpsd libgps-dev libi2c-dev libsqlitecpp-dev nlohmann-json3-dev libargs-dev linux-headers-raspi
pipx install meson
pipx install ninja
```

## Build

```bash
git clone https://git.thm.de/messdaten-radkasten/code-messdaten-cpp
cd code-messdaten-cpp
CC=/usr/bin/clang-17 CC_LD=mold CXX=/usr/bin/clang++-17 CXX_LD=mold meson setup build --buildtype=release --optimization=3 -Db_lto=true -Db_pie=true -Db_pch=true
meson compile -C build
```
