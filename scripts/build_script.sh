#!/bin/bash

# This script is used to build the project

SCRIPT_DIR="$(dirname "$0")"
PROJECT_DIR="$SCRIPT_DIR/.."

# Set the compiler and linker
export CC=clang-18
export CXX=clang++-18
export CC_LD=mold
export CXX_LD=mold

# cd to the project directory
cd $PROJECT_DIR

# Get the name of the build directory from the 1st argument
BUILD_DIR=build_auto
if [ $# -eq 1 ]; then
    BUILD_DIR=$1
fi

# Setup the build directory
meson setup $BUILD_DIR --buildtype=release --optimization=3 -Db_lto=true -Db_pie=true -Db_pch=true --unity=on

# compile the project
meson compile -C $BUILD_DIR

# Run the tests
meson test -C $BUILD_DIR

# Create zstd tarball of the
tar -caf messdaten.tar.zstd $BUILD_DIR
