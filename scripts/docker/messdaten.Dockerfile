FROM ubuntu:noble

RUN apt-get update
RUN apt-get install -y wget fish software-properties-common gpg

# Install the build tools
RUN wget https://apt.llvm.org/llvm.sh
#RUN chmod +x llvm.sh
#RUN ./llvm.sh 18 all
RUN apt-get install -y clang-18 clang++-18 gcc-13 g++-13 build-essential cmake mold meson ninja-build git

# install the dependencies
RUN apt-get install -y gpsd libgps-dev libi2c-dev libsqlitecpp-dev nlohmann-json3-dev libargs-dev
