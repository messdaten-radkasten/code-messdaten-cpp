import os
import subprocess
import shutil

if __name__ == "__main__":
    if shutil.which("docker") is None:
        raise Exception("Docker is not installed")

    # check if docker compose is installed
    if shutil.which("docker-compose") is None:
        raise Exception("Docker-compose is not installed")

    script_dir = os.path.dirname(os.path.realpath(__file__))
    docker_dir = os.path.join(script_dir, "docker")

    # Build the docker image using compose
    subprocess.call(["docker", "compose", "build"], cwd=docker_dir)

    # execute the build script inside the container
    subprocess.call([
        "docker", "compose", "run", "messdaten", "bash", "/opt/messdaten/scripts/build_script.sh", "build_docker"
    ], cwd=docker_dir)
