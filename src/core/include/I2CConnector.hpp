#pragma once

// https://medium.com/geekculture/raspberry-pi-c-libraries-for-working-with-i2c-spi-and-uart-4677f401b584
// https://github.com/milekium/spidev-lib/blob/master/CMakeLists.txt
// https://emilk.github.io/loguru/index.html#gettingstarted/aslightlymoreadvancedexample
namespace Messdaten::utils {
    class I2CConnector {
      private:
        int                                                     fd;
        uint8_t                                                   bus_id;
        std::atomic<bool> isAlive = true;
        sec_sigc::sec_signal<void, I2CConnector&> on_data;

        explicit I2CConnector(uint8_t bus_id);
        void work();

        int setup_slave(uint16_t addr);

      public:
        ~I2CConnector();
        I2CConnector(const I2CConnector&)            = delete;
        I2CConnector(I2CConnector&&)                 = delete;
        I2CConnector& operator=(const I2CConnector&) = delete;
        I2CConnector& operator=(I2CConnector&&)      = delete;

        uint8_t read_byte(uint16_t addr, uint64_t reg);
        void    write_byte(uint16_t addr, uint64_t reg, uint8_t data);

        std::vector<uint8_t> read_bytes(uint16_t addr, uint64_t reg, size_t num_bytes);
        void                 write_bytes(uint16_t addr, uint64_t reg, const std::vector<uint8_t>& data);

      private:
        static std::vector<std::tuple<std::future<void>, std::shared_ptr<I2CConnector>>> connectors;
        static std::mutex                                                                connectors_mutex;

        static sec_sigc::sec_signal<void, I2CConnector&>& get_on_data(uint8_t bus_id) {
            std::scoped_lock lock{connectors_mutex};

            for (auto& [future, connector] : connectors) {
                if (connector->bus_id == bus_id) {
                    return connector->on_data;
                }
            }

            auto new_connector = std::shared_ptr<I2CConnector>(new I2CConnector{bus_id});
            auto new_future    = std::async(std::launch::async, &I2CConnector::work, new_connector.get());

            connectors.emplace_back(std::move(new_future), std::move(new_connector));
            return std::get<std::shared_ptr<I2CConnector>>(connectors.back())->on_data;
        }

      public:
        static void connect(uint8_t bus_id, auto& data_handler) { get_on_data(bus_id).connect(data_handler); }
        static void stopAll() {
            std::scoped_lock lock{connectors_mutex};
            for (auto& [future, connector] : connectors) {
                connector->isAlive = false;
                if (future.valid()) {
                    future.wait();
                }
                connector.reset();
                connector = nullptr;
            }
            connectors.clear();
        }
    };
} // namespace Messdaten::utils
