#pragma once

namespace Messdaten::utils {
    /**
     * Dreht die Bitreihenfolge eines Integers um.
     * Diese Funktion dreht die Bitreihenfolge eines Integers um.
     *
     * :param value: Der Integer Wert.
     * :param bit_length: Die Anzahl der Bits des Integers.
     */
    template <size_t bit_count>
    auto rotate_int(std::unsigned_integral auto value) noexcept {
        decltype(value) result = 0;
        for (size_t i = 0; i < bit_count; i++) {
            result = (result << 1) | (value & 1);
            value  = value >> 1;
        }
        return result;
    }

    /**
     * Wendet das Zweierkomplement auf einen Integer an.
     * Diese Funktion interpretiert den Integer als Zweierkomplement und wendet das Zweierkomplement darauf an.
     * Wenn das bit an Stelle bit_count-1 gesetzt ist, wird der Wert als negativ interpretiert.
     * Im Falle eines positiven Wertes wird der Wert unverändert zurückgegeben.
     *
     * :param value: Der Integer Wert.
     * :param bit_count: Die Anzahl der Bits des Integers.
     */
    template <size_t bit_count>
    int64_t apply_twos_complement(uint64_t value) noexcept {
        constexpr const uint64_t twos_comp_mask = 0b1 << (bit_count - 1);
        constexpr const uint64_t inverse_mask   = 0xFFFFFFFFFFFFFFFF >> (64 - bit_count);

        if ((value & twos_comp_mask) == 0) {
            return static_cast<int64_t>(value);
        }

        return -static_cast<int64_t>(value ^ inverse_mask) - 1;
    }

    /**
     * Wandelt einen normalisierten Integer in einen Float um.
     * Diese Funktion entscheidet den Wertebereich an Hand des Template parameters, des Datentyps und ob der Wert positiv oder negativ ist.
     * Anschließend wird der Wert in den Bereich von -1.0 bis 1.0 skaliert.
     * Wenn der Datentyp unsigned ist, wird der Wert in den Bereich von 0.0 bis 1.0 skaliert und die Genauigkeit ist höher.
     * Wenn der Datentyp signed ist, wird der Wert in den Bereich von -1.0 bis 1.0 skaliert, mit der korrekten Skalierung für negative
     * Werte.
     *
     * :param value: Der Integer Wert.
     * :param bit_count: Die Anzahl der Bits des Integers.
     */
    template <size_t bit_count>
    double normalize_int(std::integral auto value) noexcept {
        if constexpr (bit_count == 0) {
            return 0.0;
        }

        if constexpr (std::is_signed_v<decltype(value)>) {
            // If the value is signed the range is from -2^(bit_count-1) to 2^(bit_count-1)-1
            // Because of this a different scaling is needed for the positive and negative values
            // Also since the first bit is the sign bit it is not used for the value and the effective bit count is bit_count-1

            constexpr const auto adjusted_bit_count = bit_count - 1;
            constexpr const auto pos_max_value      = static_cast<double>((0b1 << adjusted_bit_count) - 1);
            constexpr const auto neg_max_value      = static_cast<double>((0b1 << adjusted_bit_count));

            if (value < 0) {
                return static_cast<double>(value) / neg_max_value;
            } else {
                return static_cast<double>(value) / pos_max_value;
            }
        } else {
            // If the value is unsigned the range is from 0 to 2^(bit_count)-1
            constexpr const auto max_value = static_cast<double>((0b1 << bit_count) - 1);
            return static_cast<double>(value) / max_value;
        }
    }

    template <bool low_byte_last>
    constexpr auto start_it_getter(std::ranges::contiguous_range auto& data) {
        if constexpr (low_byte_last) {
            return std::ranges::begin(data);
        } else {
            return std::ranges::rbegin(data);
        }
    }

    template <bool low_byte_last>
    constexpr auto end_it_getter(std::ranges::contiguous_range auto& data) {
        if constexpr (low_byte_last) {
            return std::ranges::end(data);
        } else {
            return std::ranges::rend(data);
        }
    }

    /**
     * Kombiniert Bytes zu einer Zahl.
     * Diese Funktion kombiniert eine Liste von Bytes zu einer Zahl. Die Bytes müssen in der Reihenfolge, in der sie
     * kombiniert werden sollen, übergeben werden.
     *
     * :param data: Die Liste der Bytes.
     * :param bit_count: Die Anzahl der Bits, die kombiniert werden sollen.
     * :param twos_complement: Gibt an, ob das Ergebnis als Zweierkomplement interpretiert werden soll.
     * :param low_bit_first: Gibt an, ob das niederwertigste Bit zuerst kommt.
     * :param low_byte_last: Gibt an, ob das niederwertigste Byte das letzte in der Liste ist.
     * :param partial_byte_last: Das unvollständige Byte soll am Anfang oder am Ende interpretiert werden.
     * :param partial_byte_right: Das unvollständige Byte soll Rechts- oder Linksbündig interpretiert werden.
     */
    template <size_t bit_count,
              bool   twos_complement    = true,
              bool   scale_result       = false,
              bool   low_bit_first      = false,
              bool   low_byte_last      = true,
              bool   partial_byte_last  = false,
              bool   partial_byte_right = true>
    auto combine_bytes(const std::ranges::contiguous_range auto& data) {
        constexpr const auto num_bytes    = static_cast<size_t>(static_cast<double>(bit_count) / 8.0 + 1.0);
        constexpr const auto partial_bits = bit_count % 8;

        auto start_it = start_it_getter<low_byte_last>(data);
        auto end_it   = end_it_getter<low_byte_last>(data);

        decltype(start_it) partial_byte_it;
        if constexpr (partial_byte_last == low_byte_last) {
            partial_byte_it = start_it + (num_bytes - 1);
        } else {
            partial_byte_it = start_it;
        }

        uint64_t combined_value = 0;
        for (; start_it != end_it; start_it++) {
            auto value = static_cast<uint8_t>(*start_it);
            if (low_bit_first) {
                value = rotate_int<8>(value);
            }

            if constexpr (partial_bits != 0) {
                if (start_it == partial_byte_it) {
                    if (partial_byte_right) {
                        value = value & (0xFF >> partial_bits);
                    } else {
                        value = value >> (8 - partial_bits);
                    }

                    combined_value = (combined_value << partial_bits) | value;
                    continue;
                }
            }

            combined_value = (combined_value << 8) | value;
        }

        if constexpr (scale_result) {
            if constexpr (twos_complement) {
                return normalize_int<bit_count>(apply_twos_complement<bit_count>(combined_value));
            } else {
                return normalize_int<bit_count>(combined_value);
            }
        } else {
            if constexpr (twos_complement) {
                return apply_twos_complement<bit_count>(combined_value);
            } else {
                return combined_value;
            }
        }
    }
} // namespace Messdaten::utils
