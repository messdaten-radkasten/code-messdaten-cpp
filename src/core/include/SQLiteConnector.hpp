#pragma once

//Forward declarations
namespace Messdaten::sensor{
    class SensorData;
}

namespace Messdaten::utils{
    class SQLiteConnector{
      private:
        std::filesystem::path db_path;
        std::unique_ptr<SQLite::Database> db;

        decltype(std::chrono::milliseconds(100)) worker_sleep_time = std::chrono::milliseconds(100);
        decltype(std::chrono::milliseconds(1000)) worker_mutex_timeout = std::chrono::milliseconds(1000);
        std::future<void> worker_future;

        std::vector<Messdaten::sensor::SensorData> data_queue;
        std::timed_mutex data_queue_mutex;

        std::unordered_map<std::string, std::string> sql_string_cache;
        std::atomic<bool> alive = false;

        decltype(std::chrono::minutes(60)) backup_interval = std::chrono::minutes(60);
        int64_t backup_count = 10;
        std::filesystem::path backup_path;
        std::vector<std::filesystem::path> last_backup_files;
        size_t oldest_backup_index = 0;
        std::string backup_base_name;
        decltype(std::chrono::system_clock::now()) last_backup_time = std::chrono::system_clock::now();

        void worker() noexcept;
        bool backup_if_needed() noexcept;

        std::string get_sql_string(const sensor::SensorData& sensor) noexcept;
      public:
        explicit SQLiteConnector(const nlohmann::json& config);
        ~SQLiteConnector() noexcept;
        void record_data(Messdaten::sensor::SensorData&& data) noexcept;
        void close() noexcept;
    };
}