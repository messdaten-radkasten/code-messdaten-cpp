#pragma once

namespace Messdaten::utils{
    class SQLiteConnector;
}

namespace Messdaten::sensor {
    class SensorBase;

    using sensor_construc_func =
        std::function<std::unique_ptr<SensorBase>(const nlohmann::json&, std::shared_ptr<Messdaten::utils::SQLiteConnector>)>;

    class SensorLoader {
      private:
        SensorLoader() = default;

        std::unordered_map<std::string, sensor_construc_func> sensor_constructors{};

        std::unique_ptr<SensorBase> load_sensor(nlohmann::json& config, std::shared_ptr<Messdaten::utils::SQLiteConnector> db, bool log_discarded, bool log_values);

        static SensorLoader& get_instance() {
            static SensorLoader instance;
            return instance;
        }

      public:
        static std::vector<std::unique_ptr<SensorBase>> load_sensors(nlohmann::json&                                     sensorConfig,
                                                                     const std::shared_ptr<Messdaten::utils::SQLiteConnector>& db,
                                                                     bool log_discarded = false, bool log_values = false) {

            if (sensorConfig.empty()) {
                throw std::runtime_error("No sensor configuration found");
            }

            if (!sensorConfig.is_array()) {
                throw std::runtime_error("Sensor configuration must be an array");
            }

            std::vector<std::unique_ptr<SensorBase>> sensors;
            for (auto& sensor : sensorConfig) {
                try {
                    sensors.push_back(get_instance().load_sensor(sensor, db, log_discarded, log_values));
                } catch (...) {
                    std::throw_with_nested(std::runtime_error("Failed to load sensor"));
                }
            }
            return sensors;
        }

        template <class sensor_child_class>
            requires std::is_base_of_v<SensorBase, sensor_child_class>
        static void register_sensor(const std::string& name) {
            if (get_instance().sensor_constructors.contains(name)) {
                return;
            }

            get_instance().sensor_constructors[name] = [](const nlohmann::json&                              conf,
                                                          std::shared_ptr<Messdaten::utils::SQLiteConnector> db) {
                return std::make_unique<sensor_child_class>(conf, db);
            };
        }
    };
} // namespace Messdaten::sensor