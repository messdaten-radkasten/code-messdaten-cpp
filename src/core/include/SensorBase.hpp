#pragma once

namespace Messdaten::utils{
    class SQLiteConnector;
}

namespace Messdaten::sensor{
    class SensorData;

    class SensorBase{
      private:
        std::shared_ptr<Messdaten::utils::SQLiteConnector> db;
        void poll_func_ptr();
      protected:
        bool should_wait_for_gps;
        static std::atomic<bool> gps_location_found;

        std::string sensor_name;
        std::string table_name;

        bool log_values;
        bool log_discarded;

        std::atomic<bool> is_polling = false;
        std::future<void> polling_future;

        explicit SensorBase(
            const nlohmann::json& config,
            std::shared_ptr<Messdaten::utils::SQLiteConnector> db,
            bool should_wait_for_gps = true,
            std::string default_sensor_name = "unknown",
            std::string default_table_name = "unknown"
        );

        void start_polling();
        void wait_for_gps();

        virtual void poll_and_record() = 0;

        void record_data(SensorData&& data);
      public:
        SensorBase(const SensorBase&) = delete;
        SensorBase(SensorBase&&) = delete;
        SensorBase& operator=(const SensorBase&) = delete;
        SensorBase& operator=(SensorBase&&) = delete;

        virtual ~SensorBase();
        void stop_polling();
    };
}