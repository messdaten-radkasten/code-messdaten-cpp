#pragma once

#ifndef CODE_MESSDATEN_EXPORT_MACRO
    #define CODE_MESSDATEN_EXPORT_MACRO
#endif

//utility code
#include "number_utils.hpp"
#include "SQLiteConnector.hpp"
#include "I2CConnector.hpp"

//core sensor classes
#include "SensorData.hpp"
#include "SensorBase.hpp"

//the sensor loader is a bit of a special case
//all the implementations will need to register their sensors with it
#include "SensorLoader.hpp"
