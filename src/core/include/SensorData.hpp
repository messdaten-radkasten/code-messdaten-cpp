#pragma once

namespace Messdaten::sensor{
    using timestamp_type = std::chrono::utc_time<std::chrono::nanoseconds>;

    class SensorData{
      private:
        std::string table_name;
        std::string sensor_name;
        std::string port;
        timestamp_type timestamp;
        nlohmann::json data;
      public:
        explicit SensorData(
            const nlohmann::json& data,
            const std::string& table_name,
            const std::string& sensor_name,
            std::optional<timestamp_type> timestamp = {},
            const std::string& port = "N/A"
        );

        [[nodiscard]] std::string get_id() const noexcept;
        [[nodiscard]] std::string get_table_name() const noexcept;
        [[nodiscard]] std::string get_sensor_name() const noexcept;
        [[nodiscard]] std::string get_port() const noexcept;
        [[nodiscard]] timestamp_type get_timestamp() const noexcept;
        [[nodiscard]] const nlohmann::json& get_data() const noexcept;

        [[nodiscard]]
        static timestamp_type get_current_timestamp() noexcept{
            return std::chrono::utc_clock::now();
        }

        [[nodiscard]]
        static std::string create_sql_table_str(const SensorData& sensor) noexcept{
            std::stringstream ss;
            ss << "CREATE TABLE IF NOT EXISTS " << sensor.table_name << " (";
            ss << "timestamp INTEGER PRIMARY KEY NOT NULL, ";
            ss << "sensor_name TEXT NOT NULL, ";
            ss << "port TEXT NOT NULL, ";
            ss << "data BLOB NOT NULL";
            ss << ") ";
            return ss.str();
        }

        [[nodiscard]]
        static std::string create_sql_insert_str(const SensorData& sensor) noexcept{
            std::stringstream ss;
            ss << "INSERT INTO " << sensor.table_name << " (";
            ss << "timestamp, sensor_name, port, data";
            ss << ") VALUES (";
            ss << "?, ?, ?, ?";
            ss << ") ";
            return ss.str();
        }
    };
}


