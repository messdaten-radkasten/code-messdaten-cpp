/**
 * Testet die rotate_int Funktion.
 * Diese soll die bitReihenfolge eines Integers umkehren.
 * Da die Funktion nur auf einzelne Bytes aufgerufen wird, werden nur kleine Zahlen getestet.
 */
TEST (number_utils, rotate_int){
    EXPECT_EQ(Messdaten::utils::rotate_int<2>(0b01u), 0b10);
    EXPECT_EQ(Messdaten::utils::rotate_int<3>(0b010u), 0b010);
    EXPECT_EQ(Messdaten::utils::rotate_int<3>(0b100u), 0b001);
}

template<size_t bit_count, bool partial_byte_last, bool partial_byte_right>
void partial_combine(const std::vector<uint8_t>& data, auto expected){
    auto result = Messdaten::utils::combine_bytes<bit_count, false, false, false, true, partial_byte_last, partial_byte_right>(data);
    EXPECT_EQ(result, expected);
}

/**
 * Testet die combine_bytes Funktion mit 4 Bit Daten.
 * Die Funktion soll 4 Bit Daten in einen Integer umwandeln.
 * Hier wird getestet, ob die Links und Rechtsbündigkeit korrekt funktioniert.
 * Es werden hier keine Bytes getestet, die über die Grenze von 4 Bit hinausgehen.
*/
TEST (number_utils, partial_bytes_4_bit){
    std::vector<uint8_t> value1 = {0b11110000};
    std::vector<uint8_t> value2 = {0b00001111};

    // check if the data in the first half works
    partial_combine<4, true, false>(value1, 0b1111);
    partial_combine<4, false, false>(value1, 0b1111);

    // check if the data in the second half works
    partial_combine<4, true, true>(value2, 0b1111);
    partial_combine<4, false, true>(value2, 0b1111);
}

/**
 * Testet die combine_bytes Funktion mit 12 Bit Daten.
 * Die Funktion soll 12 Bit Daten in einen Integer umwandeln.
 * Hier wird getestet, ob die Links und Rechtsbündigkeit über die Grenze von 8 Bit hinaus korrekt funktioniert.
 * Außerdem wird getestet, ob die Position des partiellen Bytes korrekt ist.
 */
TEST (number_utils, test_partial_bytes_12_bit){
        uint8_t value1 = 0xF0;
        uint8_t value2 = 0x0F;

        // partial byte first
        partial_combine<12, false, false>({value1, 0xFF}, 0xFFF);
        partial_combine<12, false, true>({value2, 0xFF}, 0xFFF);

        // partial byte last
        partial_combine<12, true, false>({0xFF, value1}, 0xFFF);
        partial_combine<12, true, true>({0xFF, value2}, 0xFFF);
}

template <bool twos_complement>
void two_cmp_test(std::ranges::contiguous_range auto data, std::integral auto expected){
    std::ranges::contiguous_range auto stored = data;
    auto result = Messdaten::utils::combine_bytes<16, twos_complement>(stored);

    static_assert(std::is_same_v<decltype(result), decltype(expected)>);
    if constexpr (twos_complement){
        static_assert(std::is_same_v<decltype(result), int64_t>);
        static_assert(std::is_same_v<decltype(result), uint64_t> == false);
    }else{
        static_assert(std::is_same_v<decltype(result), uint64_t>);
        static_assert(std::is_same_v<decltype(result), int64_t> == false);
    }

    EXPECT_EQ(result, expected);
}

/**
 * Testet, ob das zweier Komplement korrekt angewendet wird.
 * Dies wird sowohl direkt in der apply_twos_complement Funktion getestet, als auch in der combine_bytes Funktion.
 * Außerdem wird getestet, ob wenn kein zweier Komplement angewendet wird, das Ergebnis korrekt ist.
 */
TEST (number_utils, test_twos_complement){

#define direct_twos_complement_test(data, expected) EXPECT_EQ(Messdaten::utils::apply_twos_complement<16>(data), expected)

    direct_twos_complement_test(0xFFFF, -1);
    direct_twos_complement_test(0x0000, 0);
    direct_twos_complement_test(0x0001, 1);
    direct_twos_complement_test(0x007F, 127);
    direct_twos_complement_test(0xFF80, -128);

#undef direct_twos_complement_test

    using arr_type = std::array<uint8_t, 2>;

    two_cmp_test<true>(arr_type{0xFF, 0xFF}, -1l);
    two_cmp_test<true>(arr_type{0x00, 0x00}, 0l);
    two_cmp_test<true>(arr_type{0x00, 0x01}, 1l);
    two_cmp_test<true>(arr_type{0x00, 0x7F}, 127l);
    two_cmp_test<true>(arr_type{0xFF, 0x80}, -128l);

    two_cmp_test<false>(arr_type{0xFF, 0xFF}, 65535ul);
    two_cmp_test<false>(arr_type{0x00, 0x00}, 0ul);
    two_cmp_test<false>(arr_type{0x00, 0x01}, 1ul);
    two_cmp_test<false>(arr_type{0x00, 0x7F}, 127ul);
    two_cmp_test<false>(arr_type{0xFF, 0x80}, 65408ul);
}

/**
 * Testet, ob die combine_bytes Funktion korrekt bytes zusammenführt.
 * Hier werden die Rückgabewerte als Hexadezimalzahlen getestet.
 * Zweier Komplement wird nicht angewendet.
 * So kann sehr einfache überprüft werden, ob die Funktion korrekt bytes zusammenführt.
 * Die Funktion wird hier nur mit 16 Bit Daten getestet.
 */
TEST (number_utils, test_simple_combine){

    const auto simple_combine = [](auto data, auto expected){
        auto result = Messdaten::utils::combine_bytes<16, false>(data);
        EXPECT_EQ(result, expected);
    };

    using arr_type = std::array<uint8_t, 2>;

    simple_combine(arr_type{0xFF, 0xFF}, 0xFFFFul);
    simple_combine(arr_type{0x00, 0x00}, 0x0000ul);
    simple_combine(arr_type{0x00, 0x01}, 0x0001ul);
    simple_combine(arr_type{0x00, 0x7F}, 0x007Ful);
    simple_combine(arr_type{0xFF, 0x80}, 0xFF80ul);
    simple_combine(arr_type{0x0F, 0xFF}, 0x0FFFul);
}

/**
 * Testet, ob die scale_result Option korrekt funktioniert.
 * Hier wird getestet, ob die Rückgabewerte korrekt normiert sind.
 */
TEST (number_utils, test_scale_result){

    const auto scale_test_ul = [](auto data, double expected){
        auto result_raw = Messdaten::utils::combine_bytes<16, false>(data);
        auto result = Messdaten::utils::normalize_int<16>(result_raw);
        EXPECT_DOUBLE_EQ(result, expected);

        auto result2 = Messdaten::utils::combine_bytes<16, false, true>(data);
        EXPECT_DOUBLE_EQ(result2, expected);
    };

    const auto scale_test_sl = [](auto data, double expected){
        auto result_raw = Messdaten::utils::combine_bytes<16, true>(data);
        auto result = Messdaten::utils::normalize_int<16>(result_raw);
        EXPECT_DOUBLE_EQ(result, expected);

        auto result2 = Messdaten::utils::combine_bytes<16, true, true>(data);
        EXPECT_DOUBLE_EQ(result2, expected);
    };

    using arr_type = std::array<uint8_t, 2>;

    scale_test_ul(arr_type{0xFF, 0xFF}, 1.0);
    scale_test_ul(arr_type{0x00, 0x00}, 0.0);
    scale_test_ul(arr_type{0x00, 0x01}, 1.0 / 65535.0);
    scale_test_sl(arr_type{0x80, 0x00}, -1.0);
    scale_test_sl(arr_type{0x00, 0x00}, 0.0);
    scale_test_sl(arr_type{0x00, 0x01}, 1.0 / 32767.0);
    scale_test_sl(arr_type{0x7F, 0xFF}, 1.0);
}


int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

