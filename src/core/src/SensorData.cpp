#include "SensorData.hpp"

Messdaten::sensor::SensorData::SensorData(
    const nlohmann::json& data,
    const std::string& table_name,
    const std::string& sensor_name,
    std::optional<timestamp_type> _timestamp,
    const std::string& port
) : table_name(table_name), sensor_name(sensor_name), port(port), data(data){

    if(_timestamp.has_value()){
        timestamp = _timestamp.value();
    }else{
        timestamp = get_current_timestamp();
    }
}

std::string Messdaten::sensor::SensorData::get_id() const noexcept{
    return table_name + "_" + sensor_name + "_" +  port;
}

std::string Messdaten::sensor::SensorData::get_table_name() const noexcept{
    return table_name;
}

std::string Messdaten::sensor::SensorData::get_sensor_name() const noexcept{
    return sensor_name;
}

std::string Messdaten::sensor::SensorData::get_port() const noexcept{
    return port;
}

Messdaten::sensor::timestamp_type Messdaten::sensor::SensorData::get_timestamp() const noexcept{
    return timestamp;
}

const nlohmann::json& Messdaten::sensor::SensorData::get_data() const noexcept{
    return data;
}

