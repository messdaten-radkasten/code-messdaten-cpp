#include "SensorBase.hpp"

#include "SensorData.hpp"
#include "SQLiteConnector.hpp"

std::atomic<bool> Messdaten::sensor::SensorBase::gps_location_found = false;

Messdaten::sensor::SensorBase::SensorBase(
    const nlohmann::json& config,
    std::shared_ptr<Messdaten::utils::SQLiteConnector> _db,
    bool _should_wait_for_gps,
    std::string default_sensor_name,
    std::string default_table_name
) : should_wait_for_gps(_should_wait_for_gps), db(_db) {
    sensor_name = SimpleGFX::json::getOptionalJsonField<std::string>(config, "sensor_name", default_sensor_name);
    table_name = SimpleGFX::json::getOptionalJsonField<std::string>(config, "table_name", default_table_name);
    log_values = config["log_values"];
    log_discarded = config["log_discarded"];
}

Messdaten::sensor::SensorBase::~SensorBase(){
    stop_polling();
}

void Messdaten::sensor::SensorBase::poll_func_ptr(){
    wait_for_gps();
    poll_and_record();
}

void Messdaten::sensor::SensorBase::start_polling(){
    if(is_polling){
        return;
    }

    is_polling = true;
    polling_future = std::async(std::launch::async, &SensorBase::poll_func_ptr, this);
}

void Messdaten::sensor::SensorBase::stop_polling(){
    if(!is_polling){
        return;
    }

    is_polling = false;
    if(polling_future.valid()){
        polling_future.wait();
    }
}

void Messdaten::sensor::SensorBase::wait_for_gps(){
    if(!should_wait_for_gps){
        return;
    }

    while(!gps_location_found){
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}

void Messdaten::sensor::SensorBase::record_data(Messdaten::sensor::SensorData&& data){
    db->record_data(std::move(data));
}

