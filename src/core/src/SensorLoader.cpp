#include "SensorBase.hpp"

#include "SensorLoader.hpp"

using namespace Messdaten;

std::unique_ptr<sensor::SensorBase> sensor::SensorLoader::load_sensor(nlohmann::json& config, std::shared_ptr<utils::SQLiteConnector> db, bool log_discarded, bool log_values){
    if (!config.contains("type")){
        throw std::runtime_error("Sensor configuration does not contain a type field");
    }

    auto type = config["type"].get<std::string>();
    std::cout << "Loading sensor of type: " << type << std::endl;

    if (!config.contains("log_discarded")){
        config["log_discarded"] = log_discarded;
    }

    if (!config.contains("log_values")){
        config["log_values"] = log_values;
    }

    if (!sensor_constructors.contains(type)){
        throw std::runtime_error("No constructor found for sensor type: " + type);
    }

    try{
        auto new_sensor = sensor_constructors[type](config, db);

        std::cout << "Finished loading sensor of type: " << type << std::endl;

        return new_sensor;
    }catch(...){
        std::throw_with_nested(std::runtime_error("Error loading sensor of type: " + type));
    }

}