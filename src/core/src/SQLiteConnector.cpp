#include "SQLiteConnector.hpp"
#include "SensorData.hpp"

using namespace SimpleGFX;
using namespace std::literals;

Messdaten::utils::SQLiteConnector::SQLiteConnector(const nlohmann::json& config){
    auto db_name = json::getOptionalJsonField<std::string>(config, "base_name", "data");
    if (json::getOptionalJsonField<bool>(config, "append_date", true)){
        auto current_date = std::chrono::system_clock::now();
        db_name += std::format("{:%Y-%m-%d}", current_date);
    }

    auto db_dir = std::filesystem::path(json::getOptionalJsonField<std::string>(config, "directory", "dataBases"));
    db_dir = std::filesystem::absolute(db_dir);
    std::filesystem::create_directories(db_dir);

    db_path = db_dir / (db_name + ".db");

    auto sleep_time = json::getOptionalJsonField<double>(config, "worker_sleep_time", 0.1);
    worker_sleep_time = std::chrono::milliseconds(static_cast<size_t>(sleep_time * 1000));

    auto mutex_timeout = json::getOptionalJsonField<double>(config, "worker_mutex_timeout", 1.0);
    worker_mutex_timeout = std::chrono::milliseconds(static_cast<size_t>(mutex_timeout * 1000));

    auto backup_interval_minutes = json::getOptionalJsonField<double>(config, "backup_interval", 60.0);
    backup_interval = std::chrono::minutes(static_cast<size_t>(backup_interval_minutes));

    backup_count = json::getOptionalJsonField<int64_t>(config, "backup_count", 10);
    if (backup_count > 0){
        last_backup_files.resize(backup_count);
    }
    auto backup_dir = json::getOptionalJsonField<std::string>(config, "backup_subdir", "backup");
    backup_path = db_dir / backup_dir;
    std::filesystem::create_directories(backup_path);
    backup_base_name = db_name;
    last_backup_time = std::chrono::system_clock::now();

    db = std::make_unique<SQLite::Database>(db_path.string().c_str(), SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE);
    alive = true;

    worker_future = std::async(std::launch::async, &SQLiteConnector::worker, this);
}

Messdaten::utils::SQLiteConnector::~SQLiteConnector() noexcept{
    close();
}

void Messdaten::utils::SQLiteConnector::close() noexcept{
    alive = false;
    worker_future.wait();
    db.reset();
}

void Messdaten::utils::SQLiteConnector::record_data(Messdaten::sensor::SensorData&& data) noexcept{
    std::scoped_lock lock(data_queue_mutex);
    data_queue.push_back(std::move(data));
}

std::string Messdaten::utils::SQLiteConnector::get_sql_string(const sensor::SensorData& sensor) noexcept{
    auto sensor_id = sensor.get_id();
    if (sql_string_cache.contains(sensor_id)){
        return sql_string_cache[sensor_id];
    }

    auto table_sql = sensor::SensorData::create_sql_table_str(sensor);
    auto insert_sql = sensor::SensorData::create_sql_insert_str(sensor);
    db->exec(table_sql);

    sql_string_cache[sensor_id] = insert_sql;
    return insert_sql;
}

void Messdaten::utils::SQLiteConnector::worker() noexcept{
    while (alive){
        if (!data_queue_mutex.try_lock_for(worker_mutex_timeout)){
            continue;
        }

        auto current_queue = std::move(data_queue);
        data_queue.clear();
        data_queue_mutex.unlock();

        for (const auto& data : current_queue){
            auto sql = get_sql_string(data);
            SQLite::Statement stmt(*db, sql);
            stmt.bind(1, data.get_timestamp().time_since_epoch().count());
            stmt.bind(2, data.get_sensor_name());
            stmt.bind(3, data.get_port());
            auto raw_data = nlohmann::json::to_msgpack(data.get_data());
            stmt.bind(4, raw_data.data(), raw_data.size());
            stmt.exec();
        }

        if (not backup_if_needed()){
            std::this_thread::sleep_for(worker_sleep_time);
        }
    }
}

bool Messdaten::utils::SQLiteConnector::backup_if_needed() noexcept{
    if (backup_count < 0){
        return false;
    }

    if (backup_interval <= 0s){
        return false;
    }

    auto current_time = std::chrono::system_clock::now();
    if (current_time - last_backup_time < backup_interval){
        return false;
    }

    last_backup_time = current_time;
    auto backup_file = backup_path / std::format("{}_{:%Y-%m-%d_%H-%M-%S}.db", backup_base_name, current_time);

    if (backup_count > 0){
        auto oldest_backup = last_backup_files[oldest_backup_index];
        if (std::filesystem::exists(oldest_backup)){
            std::filesystem::remove(oldest_backup);
        }
        last_backup_files[oldest_backup_index] = backup_file;
        oldest_backup_index = (oldest_backup_index + 1) % backup_count;
    }

    db->backup(backup_file.string().c_str(), SQLite::Database::BackupType::Save);
    return true;
}