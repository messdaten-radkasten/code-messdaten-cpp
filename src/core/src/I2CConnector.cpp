#include "I2CConnector.hpp"


std::vector<std::tuple<std::future<void>, std::shared_ptr<Messdaten::utils::I2CConnector>>> Messdaten::utils::I2CConnector::connectors{};
std::mutex                                                                Messdaten::utils::I2CConnector::connectors_mutex{};

Messdaten::utils::I2CConnector::I2CConnector(uint8_t _bus_id){
    bus_id = _bus_id;
    std::filesystem::path i2c_path{std::format("/dev/i2c-{}", bus_id)};
    fd = open(i2c_path.c_str(), O_RDWR);
    if(fd < 0){
        throw std::runtime_error("Could not open I2C device");
    }
}

Messdaten::utils::I2CConnector::~I2CConnector(){
    close(fd);
}

void Messdaten::utils::I2CConnector::work(){
    while(isAlive){
        on_data.emit(*this);
    }
}

int Messdaten::utils::I2CConnector::setup_slave(uint16_t addr){
    return ioctl(fd, I2C_SLAVE_FORCE, addr);
}

uint8_t Messdaten::utils::I2CConnector::read_byte(uint16_t addr, uint64_t reg){
    if(setup_slave(addr) < 0){
        throw std::runtime_error("Could not set up I2C slave");
    }

    auto res = i2c_smbus_read_byte_data(fd, reg);
    if(res < 0){
        throw std::runtime_error("Could not read byte from I2C device");
    }

    return static_cast<uint8_t>(res);
}

void Messdaten::utils::I2CConnector::write_byte(uint16_t addr, uint64_t reg, uint8_t data){
    if(setup_slave(addr) < 0){
        throw std::runtime_error("Could not set up I2C slave");
    }

    if(i2c_smbus_write_byte_data(fd, reg, data) < 0){
        throw std::runtime_error("Could not write byte to I2C device");
    }
}

std::vector<uint8_t> Messdaten::utils::I2CConnector::read_bytes(uint16_t addr, uint64_t reg, size_t num_bytes){
    if(setup_slave(addr) < 0){
        throw std::runtime_error("Could not set up I2C slave");
    }

    std::vector<uint8_t> res(num_bytes);
    if(i2c_smbus_read_i2c_block_data(fd, reg, num_bytes, res.data()) < 0){
        throw std::runtime_error("Could not read bytes from I2C device");
    }

    return res;
}

void Messdaten::utils::I2CConnector::write_bytes(uint16_t addr, uint64_t reg, const std::vector<uint8_t>& data){
    if(setup_slave(addr) < 0){
        throw std::runtime_error("Could not set up I2C slave");
    }

    if(i2c_smbus_write_i2c_block_data(fd, reg, data.size(), data.data()) < 0){
        throw std::runtime_error("Could not write bytes to I2C device");
    }
}
