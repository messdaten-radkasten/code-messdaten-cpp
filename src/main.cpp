#include <iostream>

int main(int argc, char* argv[]) {
    argparse::ArgumentParser program("messdatenerfassung", "0.1", argparse::default_arguments::all, true);
    program.add_description("Sammelt Daten von Sensoren und speichert sie in einer Datenbank.");

    program.add_argument("--config").default_value(std::string("config.json")).required().help("specify the output file.");

    try {
        program.parse_args(argc, argv);
    } catch (const std::exception& err) {
        std::cerr << err.what() << std::endl;
        std::cerr << program;
        return 1;
    }

    loguru::g_stderr_verbosity = 1;

    auto config_path = std::filesystem::path(program.get<std::string>("--config"));
    if (!std::filesystem::exists(config_path)) {
        std::cerr << "Config file does not exist" << std::endl;
        return 1;
    }

    Messdaten::impl::register_all_implementations();

    nlohmann::json config;
    std::ifstream  config_file(config_path);
    config_file >> config;

    if (!config.contains("dbConfig")) {
        std::cerr << "No dbConfig found in config.json" << std::endl;
        return 1;
    }

    auto sensor_config = config["sensorConfig"];
    if (!sensor_config.is_array()) {
        std::cerr << "Expected sensor configuration to be an array" << std::endl;
        return 1;
    }
    try {
        auto log_discarded = SimpleGFX::json::getJsonField<bool>(config, "log_discarded");
        auto log_values    = SimpleGFX::json::getJsonField<bool>(config, "log_values");

        std::cout << "Creating the SQLiteConnector" << std::endl;
        auto db = std::make_shared<Messdaten::utils::SQLiteConnector>(config["dbConfig"]);
        std::cout << "Finished creating the SQLiteConnector" << std::endl;

        std::cout << "Loading sensors" << std::endl;
        auto sensors = Messdaten::sensor::SensorLoader::load_sensors(sensor_config, db, log_discarded, log_values);
        std::cout << "Finished loading sensors" << std::endl;

        std::cout << "Measuring..." << std::endl;
        std::cin.get();

        std::cout << "Starting teardown" << std::endl;
        for (auto& sensor : sensors) {
            sensor->stop_polling();
        }
        sensors.clear();

        Messdaten::utils::I2CConnector::stopAll();

        db.reset();
        std::cout << "Finished teardown" << std::endl;
    } catch (const std::exception& err) {
        SimpleGFX::exception::printException(err);
    }

    return 0;
}
