// external libraries
#include "SQLiteCpp/SQLiteCpp.h"
#include "argparse/argparse.hpp"
#include "gps.h"
#include "loguru.hpp"
#include "nlohmann/json.hpp"
#include "simplegfx.hpp"

// hardware interfaces
extern "C" {
#include "i2c/smbus.h"
#include "linux/i2c-dev.h"
}

#include "spidev_lib++.h"

// required C libs
extern "C" {
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
}

// standard libraries
#include <algorithm>
#include <chrono>
#include <cmath>
#include <exception>
#include <filesystem>
#include <fstream>
#include <future>
#include <limits>
#include <optional>
#include <sstream>
#include <string>
#include <thread>
#include <tuple>
#include <utility>
#include <vector>

// group headers
#include "core.hpp"
#include "implementations.hpp"
