#include "GPSDReader.hpp"

#include <utility>

Messdaten::impl::GPSDReader::GPSDReader(const nlohmann::json& config, std::shared_ptr<Messdaten::utils::SQLiteConnector> db) : SensorBase(config, std::move(db), false, "GPSDReader", "gps_data"){

    auto poll_rate = SimpleGFX::json::getOptionalJsonField<int64_t>(config, "polling_rate");
    if (poll_rate.has_value()){
        std::cout << "Setting polling rate to " << poll_rate.value() << "Hz" << std::endl;
        auto cycle_time = 1.0 / static_cast<double>(poll_rate.value());
        try{
            std::system(std::format("gpsctl -c {}", cycle_time).c_str());
        }catch(...){
            std::cerr << "Could not set GPSD polling rate" << std::endl;
        }
    }

    if (0 != gps_open("localhost", DEFAULT_GPSD_PORT, &gps_data)) {
        throw std::runtime_error("Could not connect to GPSD");
    }

    if (gps_stream(&gps_data, WATCH_ENABLE | WATCH_JSON, NULL) != 0) {
        throw std::runtime_error("Could not start GPSD stream");
    }

    start_polling();
}

Messdaten::impl::GPSDReader::~GPSDReader(){}

void Messdaten::impl::GPSDReader::poll_and_record(){
    std::array<char, GPS_JSON_RESPONSE_MAX> buf{};
    std::array<std::string, 4> required_fields = {"lat", "lon", "alt", "time"};

    while(is_polling){
        if (gps_waiting(&gps_data, 1000000) == 0){
            continue;
        }

        auto err_code = gps_read(&gps_data, buf.data(), GPS_JSON_RESPONSE_MAX);
        if (err_code < 0){
            continue;
        }

        nlohmann::json j = nlohmann::json::parse(buf.data());
        //std::cout << j.dump(4) << std::endl;
        if (j["class"] != "TPV"){
            continue;
        }

        bool valid = true;
        for(const auto& field : required_fields){
            if(j.find(field) == j.end()){
                valid = false;
                break;
            }
        }

        if(!valid){
            continue;
        }

        if(!gps_location_found){
            gps_location_found = true;
            std::cout << "GPS location found" << std::endl;
        }

        auto port = "GPSD://" + SimpleGFX::json::getOptionalJsonField<std::string>(j, "device", "unknown");
        record_data(sensor::SensorData(j, table_name, sensor_name, {}, port));
    }

    (void)gps_stream(&gps_data, WATCH_DISABLE, NULL);
    (void)gps_close(&gps_data);
}


