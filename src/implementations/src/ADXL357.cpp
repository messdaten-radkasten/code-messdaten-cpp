#include "ADXL357.hpp"

Messdaten::impl::ADXL357::ADXL357(const nlohmann::json& config, std::shared_ptr<Messdaten::utils::SQLiteConnector> _db)
    : SensorBase(config, std::move(_db), false, "ADXL357", "adxl357_data") {

    auto range_i = SimpleGFX::json::getOptionalJsonField<int>(config, "range", 40);
    range        = static_cast<measurement_range>(range_i);
    no_temp      = SimpleGFX::json::getOptionalJsonField<bool>(config, "no_temp", false);
        
    if (!config.contains("connectionTypes")) {
        throw std::runtime_error("No connectionType found in ADXL357 configuration");
    } else {
        auto connection_types = SimpleGFX::json::getJsonField<std::string>(config, "connectionTypes");
        if (connection_types.contains("i2c")) {
            use_i2c        = true;
            i2c_bus_id     = SimpleGFX::json::getOptionalJsonField<uint8_t>(config, "i2c_bus_id", 1);
            auto str_addr  = SimpleGFX::json::getOptionalJsonField<std::string>(config, "i2c_addr", "0x1D");
            i2c_addr       = std::stoul(str_addr, nullptr, 16);
            i2c_high_speed = SimpleGFX::json::getOptionalJsonField<bool>(config, "i2c_high_speed", false);
            Messdaten::utils::I2CConnector::connect(i2c_bus_id, *this);
        }
        if (connection_types.contains("spi")) {
            use_spi    = true;
            spi_bus_id = SimpleGFX::json::getOptionalJsonField<uint8_t>(config, "spi_bus_id", 0);
            spi_cs_id  = SimpleGFX::json::getOptionalJsonField<uint8_t>(config, "spi_cs_id", 0);
            spi_speed  = SimpleGFX::json::getOptionalJsonField<uint32_t>(config, "spi_speed", 10e6);

            spi_port = std::format("/dev/spidev{}.{}", spi_bus_id, spi_cs_id);

            spi_dev  = std::make_unique<SPI>(spi_port.c_str());
            spi_port = "SPI://" + spi_port;
            spi_dev->setSpeed(spi_speed);

            if (!spi_dev->begin()) {
                throw std::runtime_error("Could not initialize SPI device");
            }

            setup_spi();
        }
    }

    if (!use_i2c && !use_spi) {
        throw std::runtime_error("No connection type specified for ADXL357");
    }

    if (use_i2c && use_spi) {
        std::cout << "Both I2C and SPI specified for ADXL357, this is not tested well but should work" << std::endl;
    }

    start_polling();
}

Messdaten::impl::ADXL357::~ADXL357() {
    this->sec_untrack();
}

void Messdaten::impl::ADXL357::poll_and_record() {
    while (is_polling) {
        if (use_spi) {
            poll_spi();
        } else {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
    if (use_spi) {
        spi_dev.reset();
        spi_dev = nullptr;
    }
}

const static uint64_t POWER_CTL_REGISTER = 0x2d;
const static uint64_t RANGE_REGISTER     = 0x2c;
const static uint64_t FILTER_REGISTER    = 0x28;
const static uint64_t RESET_REGISTER     = 0x2f;
const static uint64_t TEMP_BEGIN         = 0x06;
const static uint64_t XDATA_BEGIN        = 0x08;
// const static uint64_t YDATA_BEGIN        = 0x0B;
// const static uint64_t ZDATA_BEGIN        = 0x0E;

const static uint8_t RESET_COMMAND_VALUE = 0x52;
const static uint8_t READ_BIT            = 0x01;
const static uint8_t WRITE_BIT           = 0x00;
const static uint8_t DUMMY_BYTE          = 0xAA;

template <uint8_t command, bool write = false>
constexpr static inline uint8_t spi_command_fixed() {
    if constexpr (write) {
        return (command << 1) & ~WRITE_BIT;
    } else {
        return (command << 1) | READ_BIT;
    }
}

template <size_t dummy_amount, uint8_t command, uint8_t write = false>
constexpr static inline std::array<uint8_t, dummy_amount + 1> fill_with_dummy() {
    std::array<uint8_t, dummy_amount + 1> res{};
    std::fill(res.begin(), res.end(), DUMMY_BYTE);
    res[0] = spi_command_fixed<command, write>();
    return res;
}

static inline uint8_t get_filter_value(uint8_t low_pass, uint8_t high_pass) {
    constexpr std::array allowed_high_pass{0b000, 0b001, 0b010, 0b011, 0b100, 0b101, 0b110};
    constexpr std::array allowed_low_pass{0b0000, 0b0001, 0b0010, 0b0011, 0b0100, 0b0101, 0b0110, 0b0111, 0b1000, 0b1001, 0b1010};

    if (std::ranges::contains(allowed_high_pass, high_pass) && std::ranges::contains(allowed_low_pass, low_pass)) {
        return (high_pass << 4) | low_pass;
    } else {
        throw std::runtime_error("Invalid filter values");
    }
}

static inline uint8_t get_range_value(Messdaten::impl::ADXL357::measurement_range range, bool high_speed, bool active_high = true) {
    uint8_t range_val = 0;
    switch (range) {
        case Messdaten::impl::ADXL357::_10g:
            range_val = 0b01;
            break;
        case Messdaten::impl::ADXL357::_20g:
            range_val = 0b10;
            break;
        case Messdaten::impl::ADXL357::_40g:
            range_val = 0b11;
            break;
        default:
            throw std::runtime_error("Invalid range value");
    }

    uint8_t interrupt_polarity = active_high ? 0b1 << 6 : 0;
    uint8_t speed_mode         = high_speed ? 0b1 << 7 : 0;
    return range_val | interrupt_polarity | speed_mode;
}

static inline uint8_t get_power_value(bool no_temp, bool data_ready_off = true, bool standby = false) {
    uint8_t standby_cfg    = standby ? 0b1 : 0;
    uint8_t temp_cfg       = no_temp ? 0b10 : 0;
    uint8_t data_ready_cfg = data_ready_off ? 0b100 : 0;
    return standby_cfg | temp_cfg | data_ready_cfg;
}

template <uint8_t register_addr>
static inline void write_spi_register(SPI& spi_dev, uint8_t value) {
    constexpr const uint8_t command = spi_command_fixed<register_addr, true>();
    std::array              message{command, value};
    spi_dev.write(message.data(), 2);
}

void Messdaten::impl::ADXL357::setup_spi() {
    write_spi_register<RESET_REGISTER>(*spi_dev, RESET_COMMAND_VALUE);
    write_spi_register<FILTER_REGISTER>(*spi_dev, get_filter_value(0b0001, 0));
    write_spi_register<RANGE_REGISTER>(*spi_dev, get_range_value(range, false));
    write_spi_register<POWER_CTL_REGISTER>(*spi_dev, get_power_value(no_temp));
}

void Messdaten::impl::ADXL357::poll_spi() {
    static auto no_temp_read_buffer = fill_with_dummy<9, XDATA_BEGIN>();
    static auto temp_read_buffer    = fill_with_dummy<12, TEMP_BEGIN>();

    std::vector<uint8_t> write_buffer;
    if (no_temp) {
        write_buffer.resize(10);
        spi_dev->xfer(no_temp_read_buffer.data(), 10, write_buffer.data(), 10);
    } else {
        write_buffer.resize(12);
        spi_dev->xfer(temp_read_buffer.data(), 12, write_buffer.data(), 12);
    }

    decode_and_record(std::ranges::subrange(write_buffer.begin() + 1, write_buffer.end()), spi_port);
}

void Messdaten::impl::ADXL357::operator()(Messdaten::utils::I2CConnector& connector) {
    static bool              first     = true;
    static const std::string port_name = std::format("I2C:///dev/i2c-{}:{}", i2c_bus_id, i2c_addr);

    if (should_wait_for_gps && !gps_location_found) {
        return;
    }

    if (first) {
        first = false;
        connector.write_byte(i2c_addr, RESET_REGISTER, RESET_COMMAND_VALUE);
        connector.write_byte(i2c_addr, FILTER_REGISTER, get_filter_value(0b0001, 0));
        connector.write_byte(i2c_addr, RANGE_REGISTER, get_range_value(range, i2c_high_speed));
        connector.write_byte(i2c_addr, POWER_CTL_REGISTER, get_power_value(no_temp));
    }

    if (!this->is_polling) {
        return;
    }

    std::vector<uint8_t> buffer;
    if (no_temp) {
        buffer = connector.read_bytes(i2c_addr, XDATA_BEGIN, 9);
    } else {
        buffer = connector.read_bytes(i2c_addr, TEMP_BEGIN, 11);
    }

    decode_and_record(buffer, port_name);
}
