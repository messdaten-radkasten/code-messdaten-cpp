#include "implementations.hpp"

void Messdaten::impl::register_all_implementations(){
    Messdaten::sensor::SensorLoader::register_sensor<Messdaten::impl::GPSDReader>("GPS");
    Messdaten::sensor::SensorLoader::register_sensor<Messdaten::impl::ADXL357>("ADXL357");
}