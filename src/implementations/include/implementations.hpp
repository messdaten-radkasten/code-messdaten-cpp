#pragma once

#include "GPSDReader.hpp"
#include "ADXL357.hpp"

namespace Messdaten::impl {
    void register_all_implementations();
}