#pragma once

namespace Messdaten::impl{
    class GPSDReader : public Messdaten::sensor::SensorBase{
      private:
        struct gps_data_t gps_data;
      protected:
        void poll_and_record() override;
      public:
        explicit GPSDReader(const nlohmann::json& config, std::shared_ptr<Messdaten::utils::SQLiteConnector> db);
        ~GPSDReader() override;
    };
}
