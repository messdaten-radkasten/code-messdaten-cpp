#pragma once

namespace Messdaten::impl {
    class ADXL357 : public Messdaten::sensor::SensorBase,
                    public sec_sigc::sec_trackable,
                    public sec_sigc::func_obj<void, Messdaten::utils::I2CConnector&> {
      public:
        enum measurement_range { _10g = 10, _20g = 20, _40g = 40 };

      private:
        bool     use_i2c        = false;
        uint8_t  i2c_bus_id     = 0;
        uint16_t i2c_addr       = 0x1D;
        bool     i2c_high_speed = false;

        bool                 use_spi    = false;
        std::unique_ptr<SPI> spi_dev    = nullptr;
        uint8_t              spi_bus_id = 1;
        uint8_t              spi_cs_id  = 0;
        std::string          spi_port;
        uint32_t             spi_speed = 10e6;

        measurement_range range   = _40g;
        bool              no_temp = false;

        static inline double decode_20_bit(const std::ranges::contiguous_range auto& data) {
            return Messdaten::utils::combine_bytes<20, true, true, false, true, true, false>(data);
        }

        void decode_and_record(const std::ranges::contiguous_range auto& data, const std::string& port) {
            double temperature = 0.0;
            auto   buffer_it   = data.begin();
            auto   timestamp   = sensor::SensorData::get_current_timestamp();


            if (!no_temp) {
                temperature = Messdaten::utils::combine_bytes<16, true, true>(std::ranges::subrange(buffer_it, buffer_it + 2));
                temperature *= 25.0;
                buffer_it += 2;
            }

            auto   range_mult = static_cast<double>(range);
            double x          = decode_20_bit(std::ranges::subrange(buffer_it, buffer_it + 3)) * range_mult;
            double y          = decode_20_bit(std::ranges::subrange(buffer_it + 3, buffer_it + 6)) * range_mult;
            double z          = decode_20_bit(std::ranges::subrange(buffer_it + 6, buffer_it + 9)) * range_mult;

            record_data(sensor::SensorData{
                sensor_name,
                table_name,
                nlohmann::json{{"temperature", temperature}, {"accel_x", x}, {"accel_y", y}, {"accel_z", z}},
                timestamp,
                port
            });
        }

        void setup_spi();
        void poll_spi();

      protected:
        void poll_and_record() override;

      public:
        ADXL357(const nlohmann::json& config, std::shared_ptr<Messdaten::utils::SQLiteConnector> _db);
        ~ADXL357() override;

        void operator()(Messdaten::utils::I2CConnector& connector) override;
    };
} // namespace Messdaten::impl
